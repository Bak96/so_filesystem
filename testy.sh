if [ $1 ]
then
echo "wyniki: " > $1



unit="KB"
#zero files
a=1
while [ $a -le 100000 ]
do
	if [ $a -lt 1000 ]
	then
		unit="KB"
		aa=$a
	else
		unit="MB"
		aa=`expr $a / 1000`
	fi

	echo "create file full of 0: $aa $unit"
	echo "create file full of 0: $aa $unit" >> $1
	time dd if=/dev/zero of=/usr/test1 bs=1024 count=$a 2>> $1
	echo "copy ->"
	echo "copy ->" >> $1
	time cp /usr/test1 /usr/test2 2>> $1 
	a=`expr $a \\* 10`
done
#random files
a=1
while [ $a -le 100000 ]
do
        if [ $a -lt 1000 ]
        then
                unit="KB"
                aa=$a
        else
                unit="MB"
                aa=`expr $a / 1000`
        fi

        echo "create random file: $aa $unit"
        echo "create random: $aa $unit" >> $1
        time dd if=/dev/random of=/usr/test1 bs=1024 count=$a 2>> $1
        echo "copy ->"
        echo "copy ->" >> $1
        time cp /usr/test1 /usr/test2 2>> $1
        a=`expr $a \\* 10`
done
#2/3 random 1/3 - empty
a=10
while [ $a -le 100000 ]
do
        if [ $a -lt 1000 ]
        then
                unit="KB"
                aa=$a
        else
                unit="MB"
                aa=`expr $a / 1000`
        fi
	b=`expr $a / 3`
	echo "creating file 2/3 random 1/3 empty: $aa $unit"
	echo "creating file 2/3 random 1/3 empty: $aa $unit" >> $1
        dd if=/dev/random of=/usr/randomfile bs=1024 count=$b 2> /dev/null
	dd if=/dev/zero of=/usr/zerofile bs=1024 count=$b 2> /dev/null
	cat /usr/randomfile > /usr/test1
	cat /usr/zerofile >> /usr/test1
	cat /usr/randomfile >> /usr/test1

        echo "copy ->"
        echo "copy ->" >> $1
        time cp /usr/test1 /usr/test2 2>> $1
        a=`expr $a \\* 10`
done

echo "succesfully finished"
else
	echo Usage: $0 logfilename
fi
