wyniki: 
create file full of 0: 1 KB
1+0 records in
1+0 records out
       0.01 real       0.00 user       0.00 sys
copy ->
       0.05 real       0.01 user       0.03 sys
create file full of 0: 10 KB
10+0 records in
10+0 records out
       0.01 real       0.01 user       0.00 sys
copy ->
       0.01 real       0.00 user       0.01 sys
create file full of 0: 100 KB
100+0 records in
100+0 records out
       0.03 real       0.00 user       0.03 sys
copy ->
       0.01 real       0.00 user       0.00 sys
create file full of 0: 1 MB
1000+0 records in
1000+0 records out
       0.31 real       0.05 user       0.26 sys
copy ->
       0.06 real       0.00 user       0.05 sys
create file full of 0: 10 MB
10000+0 records in
10000+0 records out
       3.13 real       0.31 user       2.81 sys
copy ->
       0.61 real       0.00 user       0.61 sys
create file full of 0: 100 MB
100000+0 records in
100000+0 records out
      36.45 real       3.16 user      31.76 sys
copy ->
      13.31 real       0.00 user      10.76 sys
create random: 1 KB
1+0 records in
1+0 records out
       0.15 real       0.00 user       0.15 sys
copy ->
       0.15 real       0.00 user       0.15 sys
create random: 10 KB
10+0 records in
10+0 records out
       0.01 real       0.00 user       0.01 sys
copy ->
       0.00 real       0.00 user       0.00 sys
create random: 100 KB
100+0 records in
100+0 records out
       0.05 real       0.00 user       0.05 sys
copy ->
       0.01 real       0.00 user       0.01 sys
create random: 1 MB
1000+0 records in
1000+0 records out
       0.46 real       0.03 user       0.43 sys
copy ->
       0.06 real       0.01 user       0.05 sys
create random: 10 MB
10000+0 records in
10000+0 records out
       4.65 real       0.16 user       4.48 sys
copy ->
       0.85 real       0.00 user       0.83 sys
create random: 100 MB
100000+0 records in
100000+0 records out
      48.46 real       2.20 user      45.35 sys
copy ->
      12.98 real       0.06 user      10.50 sys
creating file 2/3 random 1/3 empty: 10 KB
copy ->
       0.16 real       0.00 user       0.16 sys
creating file 2/3 random 1/3 empty: 100 KB
copy ->
       0.03 real       0.00 user       0.03 sys
creating file 2/3 random 1/3 empty: 1 MB
copy ->
       0.10 real       0.00 user       0.10 sys
creating file 2/3 random 1/3 empty: 10 MB
copy ->
       1.20 real       0.00 user       0.75 sys
creating file 2/3 random 1/3 empty: 100 MB
copy ->
      11.71 real       0.05 user      10.26 sys
